
import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Juninho
 */
public class NoInstrucao {    
    int coluna;
    Instrucao value;
    ArrayList<NoInstrucao> filhos;    
    
    public NoInstrucao(int s){
        this.coluna = s;
        //this.filhos = new Instrucao(null, null, null, null);
    }
    public NoInstrucao(int s,Instrucao value){
        this.coluna = s;
        this.filhos = null;
        this.value = value;                
    }
        
    public void montarGrafoDeFluxo(ArrayReconfiguravel conf, int indexDaColuna){        
        if(indexDaColuna != conf.getAllColunas().size()){
            
            
            if(this.coluna == indexDaColuna){
                
                System.out.println("Eh a primeira vez, insira TUDO");

                for (Instrucao instr : conf.getAllColunas().get(indexDaColuna).getInstrucoes()) {
                    NoInstrucao n = new NoInstrucao(coluna, instr);
                    this.addFilho(n);
                }
                
                System.out.println("Chamo para fazer nos filhos e incremento o indice");
                this.montarGrafoDeFluxo(conf, indexDaColuna+1);

            }else{
                
                System.out.println("Nao eh a primeira vez.");
                
                if(this.filhos != null){
                    
                    System.out.println("O filho nao eh nulo");
                    
                    for (NoInstrucao filho : filhos) {
                        
                        if(verificaNohDoGrafo(filho,conf,indexDaColuna)){
                            
                            System.out.println("tem dependencia! tem que inserir e chamar para os filhos");
                            
                            adicionaColunaNoNoh(filho, conf, indexDaColuna);
                            filho.montarGrafoDeFluxo(conf, indexDaColuna+1);
                            
                        }                    
                    }
                }
            }        
        }
    }      
    
    
    @Override
    public String toString(){
        String s = "";
        
        System.out.println("Coluna : "+this.coluna);
        
        this.imprime();
        
        return s;
    }
    
    private void imprime(){
        System.out.println("estou no imprime");
        
        if(this.value != null){
            System.out.println(this.getValue().toString());        
        }
        
        if(filhos != null){
            System.out.print("\t");
            for (NoInstrucao noInstrucao : filhos) {
                noInstrucao.imprime();
            }
        }
    }
    
    public void adicionaColunaNoNoh(NoInstrucao no, ArrayReconfiguravel conf, int index){        
        for (Instrucao instrucao : conf.getAllColunas().get(index).getInstrucoes()) {
            NoInstrucao n = new NoInstrucao(index, instrucao);
            no.addFilho(n);
        }
    }
    
    public boolean verificaNohDoGrafo(NoInstrucao filho, ArrayReconfiguravel conf, int index){
        
        for (Instrucao in : conf.getAllColunas().get(index).getInstrucoes()){
            if(filho.getValue().verificaDependencia(in)){
                return true;
            }
        }
        return false;
    }
    
    public Instrucao getValue(){
        return this.value;
    }
    
    public void addFilho(NoInstrucao i){
        
        if(filhos == null){
            filhos = new ArrayList<>();
        }
        
        this.filhos.add(i);
    }
    
    public ArrayList<NoInstrucao> getFilhos(){
        return this.filhos;
    }
    
}
