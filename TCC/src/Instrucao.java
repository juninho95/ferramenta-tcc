

public class Instrucao {
	
	private String regDestino;
	private String op1;
	private String op2;
        private String address;
        
	private boolean dependencia;
	
	public Instrucao(String regDestino, String op1, String op2, String address){
		this.regDestino = regDestino.trim();
                this.dependencia = false;
		this.op1 = op1.trim();
		this.op2 = op2.trim();		
                this.address = address.trim();                        
	}
	
	public void setDependencia(boolean depend){
		this.dependencia = depend;
	}
        
	@Override
        public String toString(){
            String s = this.address+": ADD r"+regDestino+", r"+op1+", r"+op2 + "    "+ dependencia;            
            return s;            
        }
        
        public boolean verificaDependencia(Instrucao prox){
            
            if(this.regDestino.trim().compareTo(prox.getOp1().trim())==0
                    || this.regDestino.trim().compareTo(prox.getOp2().trim())== 0){
                return true;
            }            
            return false;
        }
        
	public void setRegDestino(String reg){
		this.regDestino  = reg;
	}
	
	public void setOp1(String reg){
		this.op1  = reg;
	}
	
	public void setOp2(String reg){
		this.op2  = reg;
	}
	
	public String getRegDestino(){
		return this.regDestino;		
	}
	
	public String getOp1(){
		return this.op1;		
	}
	
	public String getOp2(){
		return this.op2;		
	}
	
	public boolean isDependente(){
		return this.dependencia;		
	}
	
	
}