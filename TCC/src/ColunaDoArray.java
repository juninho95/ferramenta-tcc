


import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Juninho
 */
public class ColunaDoArray {
    private ArrayList<Instrucao> instrucoes;
    private int NumDeInstrucoes;
    
    public ColunaDoArray(){
        instrucoes = new ArrayList<>();
        NumDeInstrucoes = 0;
    }
    
    public ArrayList<Instrucao> getInstrucoes(){
        return this.instrucoes;
    }
    
    public void addInstrucao(Instrucao i){
        this.NumDeInstrucoes++;
        this.instrucoes.add(i);
    }
    
    @Override
    public String toString(){
        String s = "";
        int i;
        for (Instrucao instrucao : instrucoes) {
            s += instrucao+"\n";
        }
        return s;
    }
    
    public boolean isColunaCheia(){
        return this.NumDeInstrucoes >= 5;
    }
}
