



import java.util.ArrayList;
import java.util.HashMap;

public class GeraInstrucoes {
    
    public static ArrayList<Instrucao> constroiAssembly(int qntd) {
        int i = 1;
        ArrayList<Instrucao> s = new ArrayList<>();

        for (int j = 0; j < qntd; j++) {
            Instrucao inst = new Instrucao(String.valueOf(i).trim(), String.valueOf(i + 1).trim(), String.valueOf(i + 2).trim(), String.valueOf(j));
            s.add(inst);
            i += 3;
            if (i == 25) {
                i = 1;
            }
        }
        return s;
    }

    public static void insereDependencias(ArrayList<Instrucao> instrucoes, String[] indexes) {
        
        for (String string : indexes) {           
            String regDestino = instrucoes.get(Integer.parseInt(string.trim()) - 1).getRegDestino();
            instrucoes.get(Integer.parseInt(string.trim())).setOp1(regDestino);
            instrucoes.get(Integer.parseInt(string.trim())).setDependencia(true);            
        }
        
    }

    public static boolean podeAlocar(ColunaDoArray c, Instrucao current, int tam, ArrayList<Instrucao> naoAlocadas) {

        ArrayList<Instrucao> insts = c.getInstrucoes();
        
        //Olha as instrucoes que ja foram alocadas na coluna
        for (Instrucao instrucao : insts) {
            if (current.getOp1().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0
                    || current.getOp2().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0
                        || current.getRegDestino().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0) {
                return false;
            }
        }
        
        //Olha as instrucoes que nao foram alocadas por serem dependentes
        for(int i = 0; i < tam; i++){
            Instrucao instrucao = naoAlocadas.get(i);
            if (current.getOp1().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0
                    || current.getOp2().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0
                        || current.getRegDestino().trim().compareToIgnoreCase(instrucao.getRegDestino().trim()) == 0) {
                return false;
            }
        }
        
        
        return !c.isColunaCheia();
    }

    public static ArrayReconfiguravel alocaRecursosNoArray(ArrayList<Instrucao> assembly) {
        ArrayReconfiguravel rec = new ArrayReconfiguravel();
        ColunaDoArray col = new ColunaDoArray();
        int i = 0;
        int j = 0;

        int k = 0;

        System.out.println("iniando alocacao de recursos");
        while (!assembly.isEmpty()) {
            
            if (j == 0) {
                col.addInstrucao(assembly.get(i));
                assembly.remove(i);
                j++;
            } else {
                if (podeAlocar(col, assembly.get(i),i,assembly)) {
                    col.addInstrucao(assembly.get(i));
                    assembly.remove(i);
                } else {
                    i++;
                }
                j++;

                if (col.isColunaCheia() || j == 8 | i == assembly.size()) {
                    i = 0;
                    j = 0;
                    k++;
                    System.out.println("coluna " + k + ": \n" + col);
                    rec.addColuna(col);
                    col = new ColunaDoArray();
                }

            }
        }
        return rec;
    }
    public static void geraGrafoDeFluxoDeDados(ArrayReconfiguravel c){
        ArrayList<ColunaDoArray> colunas = c.getAllColunas();
        
        ArrayList<NoInstrucao> raizes = new ArrayList<>();
        
        int i = 0;
        
        for (ColunaDoArray colunaDoArray : colunas) {
            NoInstrucao n = new NoInstrucao(i);   
            
            System.out.println("Montando grafo da coluna "+i);
           
            n.montarGrafoDeFluxo(c,i);
            
            System.out.println("Grafo montado");
            
            raizes.add(n);
            i++;
            n.toString();            
            break;            
        }
        
        
        
        
        
    }
    
    
    public static void main(String[] args) {

        HashMap<String, String> codops = new HashMap<String, String>();
        ArrayList<Instrucao> assembly = new ArrayList<Instrucao>();

        ArrayReconfiguravel configuracao = new ArrayReconfiguravel();


        int qntdInstrucoes = Integer.parseInt(args[0]);
        String[] instrucoesDependente = args[2].split(";");

        assembly = constroiAssembly(qntdInstrucoes);

        insereDependencias(assembly, instrucoesDependente);

//        int i = 0;
//        for (Instrucao instrucoes : assembly) {
 //           System.out.println(instrucoes);
 //           i++;
 //       }
        
        System.out.println();

        configuracao = alocaRecursosNoArray(assembly);        
        System.out.println(args[2]);
        
        System.out.println("Gerando grafo de fluxo de dados");
        
       // geraGrafoDeFluxoDeDados(configuracao);



    }
}
