import java.util.HashMap;



public class GeraConf {

	private static void initHash(HashMap<String, String> c){
		c.put("nop","000000000000000000000000000000000000");
		c.put("lw","10");
		c.put("sw","01");
		c.put("add","00000");
		c.put("addi","00001");
		c.put("mult","00010");
		c.put("and","00011");
		c.put("andi","00100");
		c.put("or","00101");
		c.put("ori","00110");
		c.put("xor","00111");
		c.put("nor","01000");
		c.put("slt","01001");
		c.put("slti","01010");
		c.put("sub","01011");
		c.put("sltiu","01100");
		c.put("lui","01101");
		c.put("mfhi","01110");
		c.put("mflo","10101");
		c.put("srli","01111");
		c.put("slli","10000");
		c.put("srai","10001");
		c.put("srl","10010");
		c.put("sll","10011");
		c.put("sra","10100");		
	}
	private static String preencheZeros(String str,int tam){
		while(str.length() != tam){
			str = "0"+str;
		}
		return str;		
	}
	
	public static void main(String[] args) {
		
		
		
		HashMap<String, String> codops = new HashMap<String, String>();		
		initHash(codops);		
		int i = 0;
		int j = 1;
		
		
		for (String string : args) {
			
			if(string.contains("nop")){
				
				//System.out.println("igual");
				
				if(i%6 == 0){
					//NOP DA UNIDADE DE LW/SW
					System.out.print("0000000000000000000000000000");
				}else{
					System.out.print("000000000000000000000000000000000000");
				}
				
			}else{
				String s[] = string.split(";");
				if(s.length == 4){					
				
					String inst = s[0];						
					
					String op1 = Integer.toString(Integer.parseInt(s[1].trim()), 2);											
					String op2 = Integer.toString(Integer.parseInt(s[2].trim()),2);
					String op3 = Integer.toString(Integer.parseInt(s[3].trim()),2);
					
					op1 = preencheZeros(op1, 5);
					
					if(i%6 == 0){ //CONFIGURACAO DE LW ou SW
						
						op2 = preencheZeros(op2,16);
						op3 = preencheZeros(op3, 5);
						
						System.err.println("intru " + inst+ ":" + op1 + "_" + op3 + "_" + "_" + codops.get(inst) + "_" + op2 + " ("+s[1]+","+s[2]+","+s[3].trim()+")");
						
						System.out.print(op1+op3+codops.get(inst)+op2);
					}else{
						switch (inst) {
							//TIPO I
							case "addi":
							case "slli":
							case "ori":
							case "andi":
								op2 = preencheZeros(op2, 5);
								op3 = preencheZeros(op3, 16);
								System.err.println("instru "+ inst + ": " + op1 + "_" + op2 + "_" + "00000" + "_" +codops.get(inst)+"_"+op3+ " ("+s[1]+","+s[2]+","+s[3].trim()+")");
								System.out.print(op1+op2+"00000"+codops.get(inst)+op3);
								break;
								
							//TIPO R
							default:
								op2 = preencheZeros(op2, 5);
								op3 = preencheZeros(op3, 5);
								System.err.println("instru "+inst+": "+op1+"_"+op2+"_"+op3+"_"+codops.get(inst)+"_"+"0000000000000000" + " ("+s[1]+","+s[2]+","+s[3].trim()+")");
								System.out.print(op1+op2+op3+codops.get(inst)+"0000000000000000");
								
						}
					}
					
				}else if(s.length == 3){		//CASO DO MULT
					String inst = s[0];
					
					String op1 = Integer.toString(Integer.parseInt(s[1].trim()), 2);
					op1 = preencheZeros(op1, 5);
					
					String op2 = Integer.toString(Integer.parseInt(s[2].trim()),2);
					op2 = preencheZeros(op2, 5);
					
					System.out.print("00000"+op1+op2+codops.get(inst)+"0000000000000000");
				}else{			//CASO DOS MOVES
					
					String inst = s[0];					
					
					String op1 = Integer.toString(Integer.parseInt(s[1].trim()), 2);
					op1 = preencheZeros(op1, 5);
					
					System.out.print(op1+"00000"+"00000"+codops.get(inst)+"0000000000000000");
						
					
				}
				
			}
			
			//System.out.println(string);
			i++;
			
			if(i%6 == 0){
				System.out.println();
				System.err.println("Linha "+ j);
				j++;
			}
		}
		
	}
}


