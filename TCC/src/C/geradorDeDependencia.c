#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int isRepetido(int *lista,int tam, int inst){
	int i;
	for(i = 0; i < tam; i++){
		if(lista[i] == inst){
			return 1;
		}
	}
	return 0;
}

void sorteiaInstrucoes(int qntdInst, int numDependencia){

	int instSorteada,i;
	int tam = 0;
	int *todasAsInst;
	todasAsInst = (int *)malloc(sizeof(int)*qntdInst);

	for(i=0;i<numDependencia;i++){
		do{
			instSorteada = (rand()%(qntdInst-1)) + 1;
		}while(isRepetido(todasAsInst,tam,instSorteada) == 1);
		
		//printf("inst sorteada %d: %d\n",i+1, instSorteada);
		
		todasAsInst[tam] = instSorteada;
		tam++;
	}
	for(i = 0; i < tam-1;i++){
		printf("%d;",todasAsInst[i]);
	}
	printf("%d\n", todasAsInst[tam-1]);
}



// entrada é a porcentagem
int main(){

	int porc;
	int qntdInst;

	int numDependencia;

	scanf("%d", &qntdInst);
	scanf("%d",&porc);

	numDependencia = (qntdInst*porc)/100;

	printf("%d %d\n", qntdInst, numDependencia);
        
        srand((unsigned)time(NULL));
        
	sorteiaInstrucoes(qntdInst,numDependencia);

	



}