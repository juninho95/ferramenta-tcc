#parametro 1 nome do arquivo assembly
#parametro 2 nome do arquivo mif pra ser gerado

function trataArquivo(){

	while read linha 
	do 
		inst=$(echo $linha| awk '{print $1}');
	
		op=$(echo $linha |  awk '{print $2,$3,$4}' | sed 's/[r,i,),(]//g');
		echo $inst $op 
	done < $1
}

function gerarMif(){

	echo "WIDTH=209;
	DEPTH=1024;
	ADDRESS_RADIX=UNS;
	DATA_RADIX=BIN;
	CONTENT BEGIN" > $1;

	j=0;

	while read linha
	do
		echo  "$j 	:	0$linha;" >> $1;
		j=$(($j + 1));
	done < temp.txt

	rm temp.txt;

	echo "[$j..1023]  :   0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000;
	END;" >> $1;

}

#-==============  MAIN()
inst=$(trataArquivo $1| sed 's/ /;/g');

javac GeraConf.java;

java GeraConf $inst > temp.txt;
#cat temp.txt | sed '/^$/d' > tempNew.txt;
#rm temp

#dataToMif=$(java GeraConf $inst);
gerarMif $2;

#coloca o bit de fim de configuração
#cat $2 | tail -n 3 | head -n 1 | sed 's/:\t0/:\t1/'